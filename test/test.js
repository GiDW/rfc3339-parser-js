/* eslint-env node, mocha */

'use strict'

import assert from 'assert'

import * as gidwRFC3339Esm from '../esm/rfc3339-parser.js'
import * as gidwRFC3339EsmMin from '../esm/rfc3339-parser.min.js'
import gidwRFC3339Umd from '../umd/rfc3339-parser.js'
import gidwRFC3339UmdMin from '../umd/rfc3339-parser.min.js'

testRFC3339Parse(
  assert,
  gidwRFC3339Esm,
  'RFC3339 parser (ESM)'
)

testRFC3339Parse(
  assert,
  gidwRFC3339EsmMin,
  'RFC3339 parser (ESM) min'
)

testRFC3339Parse(
  assert,
  gidwRFC3339Umd,
  'RFC3339 parser (UMD)'
)

testRFC3339Parse(
  assert,
  gidwRFC3339UmdMin,
  'RFC3339 parser (UMD) min'
)

function testRFC3339Parse (assert, gidwRFC3339, testName) {
  describe(testName, function () {
    it('Should parse number with Date', function () {
      const input = Date.UTC(2021, 8 - 1, 12, 8, 30, 10, 234)
      const t1 = Date.UTC(2021, 8 - 1, 12, 8, 30, 10, 234)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12 correctly', function () {
      const input = '2021-08-12'
      const t1 = Date.UTC(2021, 8 - 1, 12)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12T08:30:10Z correctly', function () {
      const input = '2021-08-12T08:30:10Z'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8, 30, 10)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12 08:30:10Z correctly', function () {
      const input = '2021-08-12 08:30:10Z'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8, 30, 10)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12t08:30:10z correctly', function () {
      const input = '2021-08-12t08:30:10z'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8, 30, 10)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12T08:30:10.234Z correctly', function () {
      const input = '2021-08-12T08:30:10.234Z'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8, 30, 10, 234)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12T08:30:10.234 correctly', function () {
      const input = '2021-08-12T08:30:10.234'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8, 30, 10, 234)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12T08:30:10.234+02:00 correctly', function () {
      const input = '2021-08-12T08:30:10.234+02:00'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8 - 2, 30, 10, 234)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
    it('Should parse 2021-08-12T08:30:10.234-02:15 correctly', function () {
      const input = '2021-08-12T08:30:10.234-02:15'
      const t1 = Date.UTC(2021, 8 - 1, 12, 8 + 2, 30 + 15, 10, 234)
      assert.strictEqual(
        t1,
        gidwRFC3339.parseRFC3339(input).getTime()
      )
    })
  })
}
