/* eslint-env node */
module.exports = {
  parserOptions: {
    ecmaVersion: 5,
    sourceType: 'script'
  }
}
