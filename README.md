# RFC3339 parser

RFC3339 parser

## Usage

```javascript
import { parseRFC3339 } from '@gidw/rfc3339-parser'

const date = parseRFC3339('2021-08-12T08:30:10.234+02:00')
const time = date.getTime()
```

```javascript
var gidwRFC3339 = require('@gidw/rfc3339-parser')

const date = gidwRFC3339.parseRFC3339('2021-08-12T08:30:10.234+02:00')
const time = date.getTime()
```
